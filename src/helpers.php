<?php

if ( ! function_exists('config_path'))
{
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}

if ( ! function_exists('conf'))
{
    /**
     * Get the configuration path.
     *
     * @param  string $key
     * @param  string $defaultValue
     * @return string
     */
    function conf($key, $defaultValue = null)
    {
        $apollo = app('apollo.core');
        $ret = $apollo->get($key, $defaultValue);
        return $ret;
    }
}