<?php

/**
 * Created by PhpStorm.
 * User: li
 * Date: 2018/8/15
 * Time: 上午12:01
 */

namespace Mustafa\LaravelYar\Controllers;

use Laravel\Lumen\Routing\Controller;
use Yar_Server;

class YarController extends Controller
{
    public function load($module)
    {
        $serviceName = "App\\Http\\Controllers\\Yar\\".ucfirst($module)."Service";
        $server = new $serviceName();
        $service = new Yar_Server($server);
        $service->handle();
    }
}
