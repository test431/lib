<?php

namespace Mustafa\Apollo;

use Illuminate\Console\Command;

class ApolloCommand extends Command {
    protected $signature = "apollo:config";

    protected $description = "阿波罗配置中心拉取数据";

    public function handle(){
        $apollo = app('apollo.core');
        $apollo->run();
    }
}
