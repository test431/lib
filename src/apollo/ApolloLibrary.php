<?php

namespace Mustafa\Apollo;

class ApolloLibrary {

    private static $apolloClient = null;
    private static $instance = null;
    private static $config = [];

    /**
     * ApolloLibrary constructor.
     */
    private function __construct($nameSpace = 'xiangyi.app', $clientIp = "10.8.0.10")
    {
        self::$apolloClient = new ApolloClient(
            env("APOLLO_SERVER"),
            env("APOLLO_APPID"), [
            "application", $nameSpace
        ]);
        $config_file = self::$apolloClient->getConfigFile("application");
        if (file_exists($config_file)) {
            $config1 = (require $config_file)['configurations'];
        } else {
            $config1 = [];
        }
        $config_file = self::$apolloClient->getConfigFile($nameSpace);
        if (file_exists($config_file)) {
            $config2 = (require $config_file)['configurations'];
        } else {
            $config2 = [];
        }
        self::$config = array_merge($config1, $config2);
        (self::$apolloClient)->setClientIp($clientIp);
    }

    public static function getInstance()
    {
        if (self::$instance == null){
            self::$instance = new ApolloLibrary();
        }
        return self::$instance;
    }

    public static function run()
    {
        ini_set('memory_limit','128M');
        $pid = getmypid();
        echo "start [$pid]\n";
        $restart = true; //auto start if failed
        do {
            $error = (self::$apolloClient)->start();
            if ($error) echo('error:'.$error."\n");
            sleep(1);
        }while($error && $restart);
    }

    public static function get($key, $defaultValue = null){
        if (isset(self::$config[$key])){
            return self::$config[$key];
        }else{
            return $defaultValue;
        }
    }
}
