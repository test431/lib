<?php

namespace Mustafa\Apollo;

use Illuminate\Support\ServiceProvider;

class ApolloProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('apollo.core', function ($app) {
            return ApolloLibrary::getInstance();
        });

        $this->app->singleton('apollo.command', function ($app) {
            return new ApolloCommand();
        });

        $this->commands('apollo.command');
    }
}
