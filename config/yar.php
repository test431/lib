<?php

return [
    'yar_opt_connect_timeout' => 500,
    'yar_opt_packager' => 'msgpack',
    'yar_opt_timeout' => '1000',
];